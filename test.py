import torch

model = torch.load("models/save0.pt");

def text_pipeline(text):
    return model.vocab(model.tokenizer(text));

strings = ["race", "religion", "origin", "gender", "sexuality", "age", "disability"];
model.eval();
def test(text):
    out = model(torch.tensor(text_pipeline(text)), torch.tensor([0]));
    for label, prediction in zip(strings, out):
        print(label + ": ", prediction.item());
