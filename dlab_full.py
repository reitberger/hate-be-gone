import datasets
import torch 
import torch.nn as nn
import torch.optim as optim
import torchtext
from torchtext.data import get_tokenizer 
from torchtext.vocab import build_vocab_from_iterator
import dlab_model;


printf = print
def yield_token(data_iter):
    for text in data_iter:
        yield tokenizer(text);

dataset = datasets.load_dataset('ucberkeley-dlab/measuring-hate-speech', 'binary');
pf = dataset['train'].to_pandas();
texts = pf['text'];
target_race = pf['target_race'];
classifiers = [pf['target_race'], pf['target_religion'], pf['target_origin'], pf['target_gender'],
               pf['target_sexuality'], pf['target_age'], pf['target_disability']];

tokenizer = get_tokenizer("basic_english");
vocab = build_vocab_from_iterator(yield_token(texts), specials=["<unk>"]);
vocab.set_default_index(vocab['<unk>']);

def text_pipeline(text):
    return vocab(tokenizer(text));

vocab_size = len(vocab);
em_size = 128;
LEARNING_RATE=0.01;
EPOCHS=20;
#model = dlab_model.FullTest(vocab_size, em_size, text_pipeline);
model = torch.load("./models/test_full3.pt");

optimizer = optim.SGD(model.parameters(), lr=LEARNING_RATE);
critereon = [None] * 7;
for i in range(7):
    critereon[i] = nn.BCELoss();

def train(start, end):
    s = end - start;
    for i in range(s):
        if ( i % 1000 == 0):
            torch.save(model, "models/test_full4.pt")
            printf(i);
        i += start;
        optimizer.zero_grad();
        text = texts[i];
        model_text = torch.tensor(text_pipeline(text));
        predicted = model(model_text, torch.tensor([0]));
        loss = [None] * 7;
        for j in range(7):
            zeros = torch.zeros(1, 1);
            zeros[0][0] = bool(classifiers[j][i])
            loss[j] = critereon[j](predicted[j], zeros);
        total_loss = sum(loss);
        total_loss.backward();
        optimizer.step();

for i in range(EPOCHS):
    printf("Epoch: ");
    printf(i);
    train(50000, 130000);
torch.save(model, "models/test_full4.pt")
def test(text):
    return model(torch.tensor(text_pipeline(text)), torch.tensor([0]));
