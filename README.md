## To test this model you have to download pytorch and run `python -i test.py` and enter `test("your sentence")`
Model was trained with the [berkeley dlab dataset](https://huggingface.co/ucberkeley-dlab) and  
hacked together with pytorch's nn.EmbeddingBag   
models can be found in models while `./models/save0.pt` is the latest which has a vocab and the tokenizer
integrated. Model Blueprint is the last model defined in `dlab_model.py`
### Note how target race is used literally like the (old but liberal) sentence "Freedom for both black and white" will be classified above 90% as targeting race
### also some texts won't get rightly classified this happens to my testing the most on `target_religion`, `target_gender` and `target_sexuality` the other ones have a fairly good rate of classifying their respected probability
### Due to using a simple tokenizer, words it hadn't seen in the [dataset](https://huggingface.co/ucberkeley-dlab) will get a default token and thus a default embedding
#### For improving this it may be smarter to use something different than torch.nn.EmbeddingBag or train the BERT model for doing this task
##### This project is not quite finished though very close to it. So please keep an eye out for minor changes like refactoring
