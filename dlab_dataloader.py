import torch
import torch.nn as nn
import torch.utils.data as data
from torchtext.vocab import build_vocab_from_iterator
from torchtext.data.utils import get_tokenizer

tokenizer = get_tokenizer("basic_english");
class DlabDataset(data.Dataset):
    def __init__(self, set):
        super(DlabDataset, self).__init__();
        self.set = set;
    def __getitem__(self, idx):
        text = self.set[idx]['text'];
        row = self.set[idx];
        expected = torch.tensor([target_race(row), target_religion(row), target_origin(row), target_gender(row),\
                    target_sexuality(row), target_age(row), target_disability(row)]);
        return text_pipeline(text), expected;

#def text_pipeline(x):
#    return vocab(tokenizer(x));

def target_race(row):
    return row['target_race'];

def target_religion(row):
    return row['target_race'];

def target_origin(row):
    return row['target_origin'];

def target_gender(row):
    return row['target_gender'];

def target_sexuality(row):
    return row['target_sexuality'];

def target_age(row):
    return row['target_age'];

def target_disability(row):
    return row['target_disability'];

def annotator_ideology(row):
    return row['annotator_ideology'];

