import torch
import torch.nn as nn
class Classifier(nn.Module):
    def __init__(self, vocab_size, em_size, hidden_size, out_size):
        super(Classifier, self).__init__();
        self.embedding = nn.Embedding(vocab_size, em_size);
        self.fc0 = nn.Linear(em_size, hidden_size);
        self.fc1 = nn.Linear(hidden_size, out_size);
        self.init_weights();

    def init_weights(self, initrange=0.5):
        self.embedding.weight.data.uniform_(-initrange, initrange);
        self.fc0.weight.data.uniform_(-initrange, initrange);
        self.fc0.bias.data.zero_();
        self.fc1.weight.data.uniform_(-initrange, initrange);
        self.fc1.bias.data.zero_();

    def forward(self, text):
        embedding = self.embedding(text);
        x = self.fc0(embedding);
        x = self.fc1(x);
        return x;

class TestModel(nn.Module):
    def __init__(self, vocab_size, em_size):
        super(TestModel, self).__init__();
        self.embedding = nn.EmbeddingBag(vocab_size, em_size, mode='sum');#, mode='sum');
        self.fc0 = nn.Linear(em_size, 1);
        #self.fc1 = nn.Linear(1024, 1);
        self.init_weights();

    def init_weights(self, initrange=0.5):
        self.embedding.weight.data.uniform_(-initrange, initrange);
        self.fc0.weight.data.uniform_(-initrange, initrange);
        self.fc0.bias.data.zero_();
        #self.fc1.weight.data.uniform_(-initrange, initrange);
        #self.fc1.bias.data.zero_();


    def forward(self, x, offset):
        embedding = self.embedding(x, offset);
        x = self.fc0(embedding);
        #x = self.fc1(x);
        return torch.sigmoid(x);

class FullTest(nn.Module):
    def __init__(self, vocab_size, em_size, text_pipeline):
        super(FullTest, self).__init__();
        self.text_pipeline = text_pipeline;
        self.embedding = nn.EmbeddingBag(vocab_size, em_size, mode='sum');
        self.classes = [nn.Linear(em_size, 1),nn.Linear(em_size, 1),nn.Linear(em_size, 1),nn.Linear(em_size, 1),nn.Linear(em_size, 1),nn.Linear(em_size, 1),nn.Linear(em_size, 1),nn.Linear(em_size, 1)];
        self.num_classes = 7;

    def forward(self, text, offset):
        l = [None] * self.num_classes;
        for i in range(self.num_classes):
            l[i] = torch.sigmoid(self.classes[i](self.embedding(text, offset)));
        return l;
